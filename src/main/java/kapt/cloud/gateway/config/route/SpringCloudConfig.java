package kapt.cloud.gateway.config.route;//package prunto.cloud.gateway.config.route;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

@Configuration
public class SpringCloudConfig {

    // Commented to Use Configuration Route instead 
//    @Bean
//    public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
//        return builder.routes()
//
//                .route("kapti", r-> r.path("/users/login")
//                       // .filters(f-> f.hystrix(c -> c.setName("kapti").setFallbackUri("/users/login")))
//                        .uri("http://localhost:8081"))
//
//                .route(p -> p.path("/loan-documents")
//                                .uri("http://localhost:8081"))
//
//                .build();
//    }
}