package kapt.cloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KaptiCloudGatewayApplication {

	public static void main(String[] args) {

		SpringApplication.run(KaptiCloudGatewayApplication.class, args);
	}

}
